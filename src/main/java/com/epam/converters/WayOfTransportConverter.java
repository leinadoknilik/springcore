package com.epam.converters;

import org.springframework.core.convert.converter.Converter;

import com.epam.model.WayOfTransport;

public class WayOfTransportConverter implements Converter<String, WayOfTransport> {

	public WayOfTransport convert(final String inputText) {
		String[] values = inputText.split(",");
		String name = values[0];
		int maxSpeed= Integer.parseInt(values[1].trim());
		WayOfTransport transport = new WayOfTransport(maxSpeed);
		transport.setName(name);
		return transport;
	}
	
}
