package com.epam.model;

public class Weapon {
	private int hit;
	private String name;
	
	public Weapon(int hit) {
		super();
		this.hit = hit;
	}

	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "Weapon [hit=" + hit + ", name=" + name + "]";
	}
	
	
	
}
