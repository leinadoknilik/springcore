package com.epam.model;

public class WayOfTransport {
	private String name;
	private int maxSpeed;
	
	public WayOfTransport( int maxSpeed) {
		super();
		this.maxSpeed = maxSpeed;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Way of transport [name=" + name + "maxSpeed=" + maxSpeed + "]";
	}
	
}
