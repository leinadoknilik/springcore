package com.epam.model;

import java.util.List;

public class Character {
	public enum Role {
		HUMAN, CYBORG, GOD, SSY,ALIEN
	};

	private String name;
	private Weapon weapon;
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}



	private Role role;
	private List<Character> enemies;
	private WayOfTransport transport;

	public Character(Weapon weapon, Role role) {
		super();
		this.weapon = weapon;
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFavoriteStudents(List<Character> enemies) {
		this.enemies = enemies;
	}

	@Override
	public String toString() {
		if(role != Role.GOD){
			return "Character [name=" + name + ", weapon=" + weapon + ", role="
					+ role + ", enemies=" + enemies
					+ ", transport=" + transport + "]";
		}
		else{
			return "Character [name=" + name + ", weapon=" + weapon + ", role="
					+ role + ", transport=" + transport + "]";
		}
		
	}
	
	

	public void setTransport(WayOfTransport transport) {
		this.transport = transport;
	}

}
