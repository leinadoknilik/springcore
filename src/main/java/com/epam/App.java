package com.epam;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.epam.model.Character;

/**
 * Hello world!
 *
 */
public class App {
	private static final Logger logger = (Logger) LoggerFactory
			.getLogger(App.class);

	public static void main(String[] args) {
		final AbstractApplicationContext context = (AbstractApplicationContext) new ClassPathXmlApplicationContext(
				new String[] { "beans.xml" });

		final Map<String, Character> characters = context
				.getBeansOfType(Character.class);

		for (Map.Entry<String, Character> loopEntry : characters.entrySet()) {
			logger.info(loopEntry.getValue().toString());
		}
		context.close();
	}
}
